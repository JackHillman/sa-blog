const fs = require('fs')
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { order: ASC, fields: [frontmatter___lga, frontmatter___title] }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                lga
              }
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog posts pages.
  const posts = result.data.allMarkdownRemark.edges

  posts.forEach((post, index) => {
    const next = index === posts.length - 1 ? null : posts[index + 1].node
    const previous = index === 0 ? null : posts[index - 1].node

    createPage({
      path: post.node.fields.slug,
      component: blogPost,
      context: {
        slug: post.node.fields.slug,
        previous,
        next,
      },
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const dir = createFilePath({ node, getNode })
    const slug = dir.toLowerCase().replace(/\s+/g, '-')

    createNodeField({
      name: `slug`,
      node,
      value: slug,
    })

    let geopoly = null
    const { lga, title } = node.frontmatter
    const geopolyPath = path.join('./content/blog', lga, title, 'geopoly.json')
    if (fs.existsSync(geopolyPath)) {
      geopoly = require(path.resolve(geopolyPath))
    }

    createNodeField({
      name: `geopoly`,
      node,
      value: geopoly,
    })
  }
}
