import React, { useRef, useEffect } from 'react'
import 'mapbox-gl/dist/mapbox-gl.css'
import mapbox from 'mapbox-gl'
import { navigate } from 'gatsby'

const style = 'mapbox://styles/jackhillman/ck6lcbz5802yi1ipofh6a2oaq'

mapbox.accessToken = 'pk.eyJ1IjoiamFja2hpbGxtYW4iLCJhIjoiY2s2bGJnd2d2MDFxZzNubGZ3dnFhZ2EwcSJ9.FgPtaIduWgrIY6FWH7aSCQ'

const createPolygon = ({ geojson, properties, id }) => ({
  type: 'Feature',
  id,
  geometry: {
    type: 'Polygon',
    coordinates: geojson,
  },
  properties,
})

const addPolygon = (map, polygon) => {
  map.addSource('poly', {
    'type': 'geojson',
    'data': createPolygon(polygon),
  })

  map.addLayer({
    'id': 'poly',
    'type': 'fill',
    'source': 'poly',
    'layout': {},
    'paint': {
      'fill-color': '#088',
      'fill-opacity': 0.8,
    },
  })
}

const addPolygons = (map, polygons) => {
  const features = polygons.filter(p => p.geojson !== null).map(createPolygon)
  let hoverId = null

  map.addSource('poly', {
    type: 'geojson',
    data: {
      type: 'FeatureCollection',
      features,
    },
  })

  map.addLayer({
    'id': 'poly',
    'type': 'fill',
    'source': 'poly',
    'layout': {},
    'paint': {
      'fill-color': '#088',
      'fill-opacity': [
        'case',
        ['boolean', ['feature-state', 'hover'], false],
        0.8,
        0.2
      ],
    },
  })

  map.on('mousemove', 'poly', function(e) {
    map.getCanvas().style.cursor = 'pointer'

    if (e.features.length > 0) {
      if (hoverId) {
        map.setFeatureState(
          { source: 'poly', id: hoverId },
          { hover: false },
        )
      }

      hoverId = e.features[0].id
      map.setFeatureState(
        { source: 'poly', id: hoverId },
        { hover: true },
      )
    }
  })

  map.on('mouseleave', 'poly', function() {
    map.getCanvas().style.cursor = ''

    if (hoverId) {
      map.setFeatureState(
        { source: 'poly', id: hoverId },
        { hover: false },
      )
    }

    hoverId = null
  })

  map.on('click', 'poly', function(e) {
    const feature = e.features[0]

    if (feature) {
      const path = feature.properties.path

      if (path) {
        navigate(path, {
          state: {
            bounds: map.getBounds().toArray(),
          },
        })
      }
    }
  })
}

const Map = ({ polygon, polygons, bounds }) => {
  const container = useRef(null)
  const center = [
    138.5772652,
    -34.9088691
  ]

  useEffect(() => {
    var map = new mapbox.Map({
      container: container.current,
      style,
      center,
      zoom: 10,
    })

    if (bounds) {
      map.fitBounds(bounds)
    }

    if (polygon || polygons) {
      map.on('load', function() {
        if (polygon) {
          addPolygon(map, polygon)
        }

        if (polygons) {
          addPolygons(map, polygons)
        }
      })
    }

    return () => {
      map.remove()
    }
  })

  return (
    <div style={{
      width: '100%',
      height: 'calc(100vh - 180px)',
      position: 'relative',
    }}>
      <div style={{
        height: '100%',
        width: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
      }} ref={container} />
    </div>
  )
}

export default Map
