import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"
import Map from '../components/map'

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  const posts = data.allMarkdownRemark.edges

  const categories = posts.reduce((cats, { node }) => {
    const lga = node.frontmatter.lga
    const cat = cats[lga] || []
    cat.push(node)
    cats[lga] = cat

    return cats
  }, {})

  const polygons = posts.map(({ node }, i) => ({
    id: i,
    geojson: node.fields.geopoly,
    properties: {
      path: node.fields.slug,
    },
  }))

  const map = <Map polygons={polygons} />

  return (
    <Layout location={location} title={siteTitle} map={map}>
      <SEO title="All posts" />

      {Object.keys(categories).map((cat) => (
        <section key={cat}>
          <header>
            <h2>{cat}</h2>
          </header>

          {categories[cat].map((node) => (
            <article key={node.fields.slug}>
              <header>
                <h3
                  style={{
                    marginBottom: rhythm(1 / 4),
                  }}
                >
                  <Link style={{ boxShadow: `none` }} to={node.fields.slug}>
                    {node.frontmatter.title}
                  </Link>
                </h3>
                <small>{node.frontmatter.lga}</small>
              </header>
              <section>
                <p
                  dangerouslySetInnerHTML={{
                    __html: node.frontmatter.description || node.excerpt,
                  }}
                />
              </section>
            </article>
          ))}
        </section>
      ))}
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { order: ASC, fields: [frontmatter___lga, frontmatter___title] }
      limit: 1000
    ) {
      edges {
        node {
          id
          fields {
            slug
            geopoly
          }
          frontmatter {
            title
            lga
          }
        }
      }
    }
  }
`
